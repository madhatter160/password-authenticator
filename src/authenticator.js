var crypto = require( "crypto" );

var saltLength = {
   sha256: 256,
   sha512: 512
};

var defaultCryptoParams = {
   keyLength: 512,
   salt: null,
   iterations: 10000,
   algorithm: "sha256"
};

var hashPassword = function( password, cryptoParams, callback ) {
   "use strict";

   if ( !cryptoParams ) {
      cryptoParams = defaultCryptoParams;
   }
   else {
      if ( !cryptoParams.keyLength ) {
         cryptoParams.keyLength = defaultCryptoParams.keyLength;
      }
      if ( !cryptoParams.algorithm ) {
         cryptoParams.algorithm = defaultCryptoParams.algorithm;
      }
      if ( !cryptoParams.iterations ) {
         cryptoParams.iterations = defaultCryptoParams.iterations;
      }
   }

   if ( cryptoParams.keyLength <= 0 ) {
      if ( callback ) {
         callback( new Error( "Key length must be greater than zero." ) );
      }
      return null;
   }

   if ( cryptoParams.iterations <= 0 ) {
      if ( callback ) {
         callback( new Error( "Iterations must be greater than zero." ) );
      }
      return null;
   }

   if ( !cryptoParams.salt ) {
      var sl = saltLength[cryptoParams.algorithm];
      if ( !sl ) {
         if ( callback ) {
            callback( new Error( "Unknown algorithm specified." ) );
         }
         return null;
      }
      cryptoParams.salt = crypto.randomBytes( sl );
   }

   var hash = crypto.pbkdf2Sync( password, cryptoParams.salt, cryptoParams.iterations, cryptoParams.keyLength, cryptoParams.algorithm );

   return {
      algorithm: cryptoParams.algorithm,
      iterations: cryptoParams.iterations,
      salt: cryptoParams.salt,
      hash: hash
   };
};

var Authenticator = function() {};

Authenticator.prototype.storePassword = function( password, cryptoParams, callback ) {
   "use strict";

   var components = hashPassword( password, cryptoParams, callback );

   return components;
};

Authenticator.prototype.validatePassword = function( password, passwordComponents, callback ) {
   "use strict";

   if ( !password || !passwordComponents ) {
      return false;
   }

   var components = hashPassword( password, {
      keyLength: passwordComponents.keyLength,
      salt: passwordComponents.salt,
      iterations: passwordComponents.iterations,
      algorithm: passwordComponents.algorithm
   },
   callback );

   if ( !components ) {
      return false;
   }

   return components.hash.equals( passwordComponents.hash );
};

module.exports = new Authenticator();
