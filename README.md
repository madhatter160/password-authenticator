# **password-authenticator**
Provides an easy means of encrypting a password using pbkdf2 from the Node.JS module "crypto".  Passwords can then be checked against the encrypted password for validity.
***
# Usage
Usage is easy.  Just drop the authenticator.js file into your project and require it.  

    var pwauth = require( "authenticator" );
    
    var password = "foobar";
    var invalid = "raboof";
    
    // Encrypt the password.
    var encrypted = pwauth.storePassword( password );

    //
    // Check a submitted password against an encrypted password.
    //
    
    var ok = pwauth.validatePassword( password, encrypted );
    
    // Password is valid.
    console.log( "Password \"" + password + "\" is " + ( ok ? "valid" : "invalid" ) );
    
    // Password is invalid.
    ok = pwauth.validatePassword( invalid, encrypted );
    console.log( "Password \"" + invalid + "\" is " + ( ok ? "valid" : "invalid" ) );

# API Documentation
The following functions are available from password-authenticator:
***
## **storePassword( passwordString [, cryptoParams] [, callback] )**
Hashes a password string and returns the hashed password and the arguments used to generate the hash.
### Arguments
- passwordString: The raw password to encrypt.
- cryptoParams: Optional parameters specifying the arguments for the encryption.
    - algorithm: String specifying the algorithm pbkdf2 should use.  Valid values are: "sha256" and "sha512".  Default is "sha256".
    - iterations: Number specifying how many iterations pbkdf2 should perform.  Default is 10000.
    - keyLength: Length of the resulting key in bits.  Default is 512.
    - salt: Buffer (or hex string) containing salt to use when encrypting password. If not specified, a cryptographically secure salt of an appropriate length will be generated.
- callback: Function that receives the specific error if the storePassword fails.
    - err: Error object containing information on what caused the function to fail.
### Returns
Object containing the parameters used to encrypt the password, or null on failure.  

- algorithm: String containing the algorithm used to hash the password.  
- hash: Buffer containing the hashed password.
- iterations: Number of iterations.
- salt: Buffer containing the salt added to the password prior to hashing.
## **validatePassword( passwordString [, encryptParams] [, callback] )**
Validates a given password against a hashed password returned by storePassword.
### Arguments
- passwordString: The raw password to validate.
- encryptParams: Object containing the parameters used to encrypt the original password.
    - algorithm: The algorithm used to hash the password.
    - hash: The hashed password.
    - iterations: Number of iterations.
    - salt: The salt added to the password prior to hashing.
- callback: Function that receives the specific error if the storePassword fails.
    - err: Error object containing information on what caused the function to fail.
### Returns
True if the given password is valid, false otherwise.