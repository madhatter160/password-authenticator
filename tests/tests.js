var authenticator = require( "../src/authenticator" );

exports.testFailValidateInvalidPassword = function( test ) {
   var password = "foobar";
   var invalid = "foobar1";
   var hash = authenticator.storePassword( password );
   test.ok( !authenticator.validatePassword( invalid, hash ) );
   test.done();
};

exports.testFailValidateEmptyPassword = function( test ) {
   var password = "foobar";
   var hash = authenticator.storePassword( password );
   test.ok( !authenticator.validatePassword( "", hash ) );
   test.done();
};

exports.testFailValidateNullPassword = function( test ) {
   var password = "foobar";
   var hash = authenticator.storePassword( password );
   test.ok( !authenticator.validatePassword( null, hash ) );
   test.done();
};

exports.testFailValidateNullComponents = function( test ) {
   var password = "foobar";
   test.ok( !authenticator.validatePassword( password, null ) );
   test.done();
};

exports.testFailValidateNullPasswordAndComponents = function( test ) {
   test.ok( !authenticator.validatePassword( null, null ) );
   test.done();
};

exports.testFailValidateEmptyPasswordAndNullComponents = function( test ) {
   test.ok( !authenticator.validatePassword( "", null ) );
   test.done();
};

exports.testPassValidPassword = function( test ) {
   var password = "foobar";
   var valid = "foobar";
   var hash = authenticator.storePassword( password );
   test.ok( authenticator.validatePassword( valid, hash ) );
   test.done();
};

exports.testPassStoreKeyLengthZero = function( test ) {
   var password = "foobar";
   var valid = "foobar";
   var hash = authenticator.storePassword( password, {keyLength: 0} );
   test.ok( authenticator.validatePassword( valid, hash ) );
   test.done();
};

exports.testFailStoreKeyLengthNegative = function( test ) {
   var password = "foobar";
   test.ok( !authenticator.storePassword( password, {keyLength: -1} ) );
   test.done();
};

exports.testFailStoreSaltNegative = function( test ) {
   var password = "foobar";
   test.throws( function() {
      authenticator.storePassword( password, {salt: -1} )
   } );
   test.done();
};

exports.testPassStoreSaltZero = function( test ) {
   var password = "foobar";
   var valid = "foobar";
   var hash = authenticator.storePassword( password, {salt: 0} );
   test.ok( authenticator.validatePassword( valid, hash ) );
   test.done();
};

exports.testPassStoreIterationsZero = function( test ) {
   var password = "foobar";
   var valid = "foobar";
   var hash = authenticator.storePassword( password, {iterations: 0} );
   test.ok( authenticator.validatePassword( valid, hash ) );
   test.done();
};

exports.testFailStoreIterationsNegative = function( test ) {
   var password = "foobar";
   test.ok( !authenticator.storePassword( password, {iterations: -1} ) );
   test.done();
};

exports.testFailStoreInvalidAlgorithm = function( test ) {
   var password = "foobar";
   test.ok( !authenticator.storePassword( password, {algorithm: "sha999"} ) );
   test.done();
};
